//
//  ContentView.swift
//  CalculadoraSwiftUI
//
//  Created by Carlos Vera Baca on 20/05/21.
//

import SwiftUI

struct ContentView: View {
    
    @State var resultado = Resultado()  // contiene el texto, numeros y operacion a realizar
    @State var digitando = true
    @State var operadorPresionado: Operaciones = Operaciones.resultado

    var body: some View {
        ZStack{
            Color.black
            VStack{
                Spacer()
                TextoDeCalculadora(resultado: resultado)
                    .padding(.horizontal, 30)
                HStack{
                    AccionesCalculadora(digitando: $digitando, accion: .ac, resultado: $resultado)
                    AccionesCalculadora(digitando: $digitando, accion: .invertirSigno, resultado: $resultado)
                    AccionesCalculadora(digitando: $digitando, accion: .porcentaje, resultado: $resultado)
                    OperacionesCalculadora(digitando: $digitando, presionado: $operadorPresionado, operacion: .division, resultado: $resultado)
                }
                HStack{
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "7")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "8")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "9")
                    OperacionesCalculadora(digitando: $digitando, presionado: $operadorPresionado, operacion: .multiplicacion, resultado: $resultado)

                }
                HStack{
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "4")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "5")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "6")
                    OperacionesCalculadora(digitando: $digitando, presionado: $operadorPresionado, operacion: .resta, resultado: $resultado)

                }
                HStack{
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "1")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "2")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "3")
                    OperacionesCalculadora(digitando: $digitando, presionado: $operadorPresionado, operacion: .suma, resultado: $resultado)

                }
                HStack{
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: "0")
                    DigitoCalculadora(digitando: $digitando, resultado: $resultado, digito: ".")
                    OperacionesCalculadora(digitando: $digitando, presionado: $operadorPresionado, operacion: .resultado, resultado: $resultado)
                }
                Spacer()
            }
        }
        .font(.largeTitle)
            
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

// MARK: Vistas: textos, botones

struct TextoDeCalculadora: View {
    
    var resultado: Resultado
    
    var body: some View {
        Text(resultado.textoDeCalculadora)
            .frame(maxWidth: .infinity, maxHeight: 70, alignment: .trailing)
            .font(.system(size: 60))
            .foregroundColor(Color.white)
            .background(Color.black)
            .lineLimit(1)
            .minimumScaleFactor(0.2)
    }
}

struct AccionesCalculadora: View {
    
    @Binding var digitando: Bool
    var accion: Acciones
    @Binding var resultado: Resultado

    var body: some View {
                
        Button(action: {
            if accion == Acciones.ac {
                digitando = true
            }
            resultado = accion.realizarAccion(resultado: resultado)
        }, label: {
            Text(accion.textoAccion())
                .foregroundColor(.white)
                .frame(width: 70, height: 70)
                .background(Color(UIColor.lightGray))
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .padding(2)
        })
    }
}

struct DigitoCalculadora: View {
    
    @Binding var digitando: Bool
    @Binding var resultado: Resultado
    var digito: String
    
    var body: some View {
        Button(action: {
            
            if !digitando {
                digitando = true
                resultado.textoDeCalculadora = "0"
            }
            
            resultado = construyeTexto(resultado: resultado, digito: digito)
            
        }, label: {
            Text(digito)
                .foregroundColor(.white)
                .frame(width: digito == "0" ? 152: 70, height: 70)
                .background(Color(UIColor.darkGray))
                .clipShape(Capsule())
                .padding(2)
        })
    }
}

struct OperacionesCalculadora: View {
    
    @Binding var digitando: Bool
    @Binding var presionado: Operaciones
    var operacion: Operaciones
    @Binding var resultado: Resultado
    
    var body: some View {
                
        Button(action: {
            digitando = false
            presionado = operacion
            resultado = operacion.operar(resultado: resultado)
        }, label: {
            Text(operacion.textoOperacion())
                //.foregroundColor(.white)
                .foregroundColor(!digitando && presionado == operacion && operacion != .resultado ? .orange : .white )
                .frame(width: 70, height: 70)
                .background(Color(!digitando && presionado == operacion && operacion != .resultado ?  UIColor.white : UIColor.orange))
                //.background(Color(UIColor.orange))

                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .padding(2)
        })
    }
}

// MARK: Funciones
func construyeTexto(resultado: Resultado, digito: String) -> Resultado {
    var sTexto = resultado.textoDeCalculadora
    
    var sTextoAux = ""
    
    guard !(sTexto.contains(".") && digito == ".") else {
        return resultado
    }
    
    if (sTexto == "0" && digito != ".") {
        sTexto = ""
    }
    
    sTextoAux = sTexto + digito
    
    if Double(sTextoAux) != nil {
        sTexto = sTextoAux
    }
        
    return Resultado(textoDeCalculadora: sTexto, operacionPrevia: resultado.operacionPrevia, operacionActual: resultado.operacionActual, primerNumero: resultado.primerNumero, segundoNumero: resultado.segundoNumero)
}

// MARK: Enums
enum Acciones {
    case ac
    case invertirSigno
    case porcentaje
    
    func textoAccion() -> String {
        var simbolo: String
        switch self {
        case .ac:
            simbolo = "AC"
        case .invertirSigno:
            simbolo = "+/-"
        case .porcentaje:
            simbolo = "%"
        }
        return simbolo
    }
    
    func realizarAccion(resultado: Resultado) -> Resultado {
        
        var sTexto = resultado.textoDeCalculadora
        var primerNumero = resultado.primerNumero
        var segundoNumero = resultado.segundoNumero
        var operacionPrevia = resultado.operacionPrevia
        var operacionActual = resultado.operacionActual
        
        guard sTexto != "ERROR" || self == .ac else {
            return Resultado(textoDeCalculadora: sTexto, operacionPrevia: operacionPrevia, operacionActual: operacionActual, primerNumero: primerNumero, segundoNumero: segundoNumero)
        }
        
        switch self {
        case .ac:
            sTexto = "0"
            primerNumero = 0
            segundoNumero = 0
            operacionPrevia = .ninguna
            operacionActual = .ninguna
        case .invertirSigno:
            if sTexto.first == "-" {
                sTexto.removeFirst()
            } else {
                sTexto = "-" + sTexto
            }
        case .porcentaje:
            if let numero = Double(sTexto) {
                
                let nResultado = OperacionSimple.division
                let nRpta = nResultado.operar(numeroA: numero, numeroB: 100)
                
                sTexto = String(nRpta).formateado()
                
            } else {
                sTexto = "0"
            }
        }
        
        return Resultado(textoDeCalculadora: sTexto, operacionPrevia: operacionPrevia, operacionActual: operacionActual, primerNumero: primerNumero, segundoNumero: segundoNumero)
    }
}

enum Operaciones {
    case division
    case suma
    case resta
    case multiplicacion
    case resultado
    
    func textoOperacion() -> String {
        var simbolo: String
        switch self {
        case .division:
            simbolo = "÷"
        case .suma:
            simbolo = "+"
        case .resta:
            simbolo = "-"
        case .multiplicacion:
            simbolo = "x"
        case .resultado:
            simbolo = "="
        }
        return simbolo
    }
    
    func operar(resultado: Resultado) -> Resultado {
        var sTexto  = resultado.textoDeCalculadora
        var numero1 = resultado.primerNumero
        var numero2 = resultado.segundoNumero
        var operacionSimplePrevia = resultado.operacionPrevia
        var operacionSimpleActual = resultado.operacionActual
        
        var operacion: OperacionSimple // operacion presionada
        
        guard sTexto != "ERROR" else {
            return Resultado(textoDeCalculadora: sTexto, operacionPrevia: operacionSimplePrevia, operacionActual: operacionSimpleActual, primerNumero: numero1, segundoNumero: numero2)
        }
        
        switch self {
        case .division:
            operacion = .division
        case .suma:
            operacion = .suma
        case .resta:
            operacion = .resta
        case .multiplicacion:
            operacion = .multiplicacion
        case .resultado:
            operacion = .ninguna     // el igual (=) no es un operador
        }

        if self == .resultado {   // Presiono =
            
            if operacionSimplePrevia != .ninguna { // antes presiono un operador
                
                operacionSimplePrevia = .ninguna
                
                // numero1 = numero1
                numero2 = Double(sTexto)!
                
            } else {
                //numero2 = numero2

                numero1 = Double(sTexto)!

            }
            
            
            if operacionSimpleActual == .division && numero2 == 0 {
                sTexto = "ERROR"
            } else {
                
                if operacionSimpleActual != .ninguna {
                
                    let rpta = operacionSimpleActual.operar(numeroA: numero1, numeroB: numero2)
                    
                    //sTexto = formatearNumero(numero: rpta)
                    sTexto = String(rpta)
                }
                
                sTexto = sTexto.formateado()
                
            }
            
        } else {                // presiono una operacion
            
            
            
            numero1 = Double(sTexto)!
            numero2 = Double(sTexto)!
            
            operacionSimplePrevia = operacion
            operacionSimpleActual = operacion
                    
        }
        
        return Resultado(textoDeCalculadora: sTexto, operacionPrevia: operacionSimplePrevia, operacionActual: operacionSimpleActual, primerNumero: numero1, segundoNumero: numero2)
    }
}

enum OperacionSimple: String {
    case suma
    case resta
    case multiplicacion
    case division
    case ninguna
    
    func operar(numeroA a:Double, numeroB b: Double) -> Double {
        switch self {
        case .suma:
            return a + b
        case .resta:
            return a - b
        case .multiplicacion:
            return a * b
        case .division:
            return a / b
        case .ninguna:
            return b
        }
    }
}

// MARK: Structs: Datos agrupados

struct Resultado {
    var textoDeCalculadora: String
    var operacionPrevia: OperacionSimple
    var operacionActual: OperacionSimple
    var primerNumero: Double
    var segundoNumero: Double
    
    init() {
        textoDeCalculadora = "0"
        operacionPrevia = .ninguna
        operacionActual = .ninguna
        primerNumero = 0
        segundoNumero = 0
    }
    
    init(textoDeCalculadora: String, operacionPrevia: OperacionSimple, operacionActual: OperacionSimple, primerNumero: Double, segundoNumero: Double ) {
        self.textoDeCalculadora = textoDeCalculadora
        self.operacionPrevia = operacionPrevia
        self.operacionActual = operacionActual
        self.primerNumero = primerNumero
        self.segundoNumero = segundoNumero
    }
}

// MARK: Extensiones
extension String {
    func formateado () -> String {
        var sTexto: String

        guard let nDouble = Double(self) else {
            return "ERROR"
        }
        
        let nInt = Int(nDouble)
        
        if nDouble == Double(nInt) {
            sTexto = String(nInt)
        } else {
            sTexto = String(nDouble)
        }
        
        if self == "0." {
            sTexto = "0"
        }
        
        return sTexto
        
    }
}
