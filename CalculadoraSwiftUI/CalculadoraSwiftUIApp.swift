//
//  CalculadoraSwiftUIApp.swift
//  CalculadoraSwiftUI
//
//  Created by Carlos Vera Baca on 20/05/21.
//

import SwiftUI

@main
struct CalculadoraSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
